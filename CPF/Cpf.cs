﻿using System.Linq;
using System.Text.RegularExpressions;

namespace CPF
{
    public class Cpf
    {
        private const string RegexCpf = "^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$";
        private const string RegexRemoveMascara = @"[\.]+|[-]";

        public bool ValidarCadeia(string cadeia)
        {
            return Regex.IsMatch(cadeia, RegexCpf);
        }
        
        public int[] ParaInterios(string cadeias)
        {
            return Regex.Replace(cadeias,RegexRemoveMascara,"")
                .Select(c => int.Parse(c.ToString())).ToArray();
           
        }

        public int SomaProdutos(int[] ints)
        {
           
        }
    }
}