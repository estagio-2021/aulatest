# Testes

## Teste de unidade
São os testes feito em cima do codigo que voce escreve e que está sobre seu controle

## Criando o projeto

Criar uma solução
```shell 
dotnet new sln -o projeto_validar_número_contribuinte
```
criar um projeto
```shell
dotnet new classlib -o CPF
```
criar um projeto de teste
```shell
dotnet new xunit -o CPF.Tests
```

add os projetos a solução
```shell

dotnet sln add ./CPF/CPF.csproj

dotnet sln add ./CPF.Tests/CPF.Tests.csproj
```

## Calcular digito verificador cpf

* 111.444.777-05

Multiplicar os 9 Primeiros dígitos por um sequencia de 10 a 9

|1 |1 |1 |4 |4 |4 |7 |7 |7 |  |
---|---|---|---|---|---|---|---|---|---|
10|    9| 8| 7| 6| 5| 4| 3| 2| * |
10|    9| 8| 28|    24|    20|    28|    21|    14| = |

somar 10+9+8+28+24+20+28+21+14 = 162

162 / 11  =    14  com resto 8

- Se o resto da divisão for menor que 2, então o dígito é igual a 0 (Zero).
- Se o resto da divisão for maior ou igual a 2, então o dígito verificador é igual a 11 menos o resto da divisão (11 - resto).

No nosso exemplo temos que o resto é 8 então faremos 11-8 = 3

Logo o primeiro dígito verificador é 3. Então podemos escrever o CPF com os dois dígitos calculados :  111.444.777-3X



1| 1| 1| 4| 4| 4| 7| 7| 7| 3||
---|---|---|---|---|---|---|---|---|---|---|
11|    10|    9| 8| 7| 6| 5| 4| 3| 2|*|
11|    10|    9| 32|    28|    24|    35|    28|    21|    6|=|

soma 11 + 10 + 9 + 32 + 28 + 24 + 35 + 28 + 21 + 6 = 204

204 / 11  =  18  e  resto 6

- Se o resto da divisão for menor que 2, então o dígito é igual a 0 (Zero).
- Se o resto da divisão for maior ou igual a 2, então o dígito é igual a 11 menos o resto da divisão (11 - resto).

11-6= 5   logo 5 é o nosso segundo dígito verificador.

Logo o nosso CPF fictício será igual a : 111.444.777-35.
## Fact
```c#
[fact]
```



```c#
[Theory]
[InlineData(-1)]
[InlineData(0)]
[InlineData(1)]
```


Fonte

[Tdd Uncle Bob](https://sites.google.com/site/unclebobconsultingllc/tdd-with-acceptance-tests-and-unit-tests)

[Sobe teste de aplicacaco microsoft](https://docs.microsoft.com/pt-br/dotnet/core/testing/)

[Boas Praticas microsoft](https://docs.microsoft.com/pt-br/dotnet/core/testing/unit-testing-best-practices)

[Microsoft fact theory](https://docs.microsoft.com/pt-br/dotnet/core/testing/unit-testing-with-dotnet-test)

[Macoratti algoritimo](http://www.macoratti.net/alg_cpf.html)

