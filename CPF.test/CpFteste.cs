using Xunit;

namespace CPF.test
{
    public class CpFteste
    {
        [Theory]
        [InlineData("688.963.110-20")]
        [InlineData("602.725.750-46")]
        [InlineData("791.549.440-66")]
        public void Cpfcomtamanhovalido(string cadeias)
        {
            Assert.True(new Cpf().ValidarCadeia(cadeias));
        }
        
        [Theory]
        [InlineData("a")]
        [InlineData("11111111111111111111")]
        [InlineData("----------")]
        public void Cpfcomtamanhoinvalido(string cadeias)
        {
            Assert.False(new Cpf().ValidarCadeia(cadeias));
        }
        
        [Theory]
        [InlineData("688.963.110-20",new [] {6,8,8,9,6,3,1,1,0,2,0})]
        [InlineData("602.725.750-46",new [] {6,0,2,7,2,5,7,5,0,4,6})]
        [InlineData("791.549.440-66",new [] {7,9,1,5,4,9,4,4,0,6,6})]
        [InlineData("10055523048",new [] {1,0,0,5,5,5,2,3,0,4,8})]
        public void CadeiaValidaParaInteiro(string cadeias, int[] ints)
        {
           Assert.Equal(ints, new Cpf().ParaInterios(cadeias));
        }
        
        [Theory]
        [InlineData(new [] {6,8,8,9,6,3,1,1,0,2,0},317)]
        [InlineData(new [] {6,0,2,7,2,5,7,5,0,4,6},205)]
        [InlineData(new [] {7,9,1,5,4,9,4,4,0,6,6},291)]
        [InlineData(new [] {1,0,0,5,5,5,2,3,0,4,8},117)]
        public void CalcularSomaDosProdutos(int[] ints, int soma)
        {
            Assert.Equal(soma, new Cpf().SomaProdutos(ints));
        }
    }
}